<?php
session_start();
include("include/functions.php");
if (isset($_GET['reset'])) {
    unset($_SESSION['editid']);
}
?>

<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8">
    <title>Kalbų mokymosi sistema</title>
    <link href="include/styles.css" rel="stylesheet" type="text/css">
</head>

<body>
    <table class="center">
        <tr>
            <td>
                <center>
                    <h1>Kalbų mokymosi sistema</h1>
                </center>
            </td>
        </tr>
        <tr>
            <td>
                <?php

                if (!empty($_SESSION['user'])) {

                    inisession("part");
                    $_SESSION['prev'] = "index";

                    include("include/meniu.php");
                    include("app/home.php");
                ?>




                <!-- <div style="text-align: center;color:green">
                        <br><br>
                        <h1>Pradinis sistemos puslapis (index.php).</h1>
                    </div><br> -->
                <?php
                } else {

                    if (!isset($_SESSION['prev'])) inisession("full");             // nustatom sesijos kintamuju pradines reiksmes 
                    else {
                        if ($_SESSION['prev'] != "proclogin") inisession("part"); // nustatom pradines reiksmes formoms
                    }
                    // jei ankstesnis puslapis perdavė $_SESSION['message']
                    echo "<div align=\"center\">";
                    echo "<font size=\"4\" color=\"#ff0000\">" . $_SESSION['message'] . "<br></font>";

                    echo "<table class=\"center\"><tr><td>";
                    include("include/login.php");                    // prisijungimo forma
                    echo "</td></tr></table></div><br>";
                }
                ?>
</body>

</html>