<?php
// meniu.php  rodomas meniu pagal vartotojo rolę

if (!isset($_SESSION)) {
    header("Location: logout.php");
    exit;
}
include("include/nustatymai.php");
$user = $_SESSION['user'];
$userlevel = $_SESSION['ulevel'];
$role = ""; {
    foreach ($user_roles as $x => $x_value) {
        if ($x_value == $userlevel) $role = $x;
    }
}

echo "<table width=100% border=\"0\" cellspacing=\"1\" cellpadding=\"3\" class=\"meniu\">";
echo "<tr><td>";
echo "Prisijungęs vartotojas: <b>" . $user . "</b>     Rolė: <b>" . $role . "</b> <br>";
echo "</td></tr><tr><td>";
if ($_SESSION['user'] != "guest") echo "[<a href=\"useredit.php\">Redaguoti paskyrą</a>] &nbsp;&nbsp;";
if ($userlevel == $user_roles[DEFAULT_LEVEL]) {
    echo "[<a href=\"app/report.php\">Peržiūrėti raportą</a>] &nbsp;&nbsp;";
    echo "[<a href=\"app/dict_list.php\">Mano žodynai</a>] &nbsp;&nbsp;";
    echo "[<a href=\"app/add_dict.php\">Kurti žodyną</a>] &nbsp;&nbsp;";
}
if ($userlevel == $user_roles[CREATOR_LEVEL]) {
    echo "[<a href=\"app/creator_dict_list.php\">Redaguoti žodynus</a>] &nbsp;&nbsp;";
    echo "[<a href=\"app/creator_add_dict.php\">Kurti žodyną</a>] &nbsp;&nbsp;";
}

if ($userlevel == $user_roles[ADMIN_LEVEL]) {
    echo "[<a href=\"admin.php\">Administratoriaus sąsaja</a>] &nbsp;&nbsp;";
}
echo "[<a href=\"logout.php\">Atsijungti</a>]";
echo "</td></tr></table>";