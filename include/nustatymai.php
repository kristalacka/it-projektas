<?php
//nustatymai.php
define("DB_SERVER", "localhost");
define("DB_USER", "stud");
define("DB_PASS", "stud");
define("DB_NAME", "language_app2");
define("TBL_USERS", "User");
$user_roles = array(      // vartotojų rolių vardai lentelėse ir  atitinkamos userlevel reikšmės
	"Admin" => "9",
	"DictionaryCreator" => "5",
	"User" => "4",
);   // galioja ir vartotojas "guest", kuris neturi userlevel
define("DEFAULT_LEVEL", "User");  // kokia rolė priskiriama kai registruojasi
define("CREATOR_LEVEL", "DictionaryCreator");
define("ADMIN_LEVEL", "Admin");  // kas turi vartotojų valdymo teisę
define("UZBLOKUOTAS", "255");      // vartotojas negali prisijungti kol administratorius nepakeis rolės
$uregister = "both";  // kaip registruojami vartotojai
// self - pats registruojasi, admin - tik ADMIN_LEVEL, both - abu atvejai
// * Email Constants - 
define("EMAIL_FROM_NAME", "Demo");
define("EMAIL_FROM_ADDR", "demo@ktu.lt");
define("EMAIL_WELCOME", false);