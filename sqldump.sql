-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 02, 2020 at 03:51 PM
-- Server version: 5.7.32-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `language_app2`
--

-- --------------------------------------------------------

--
-- Table structure for table `Dictionary`
--

CREATE TABLE `Dictionary` (
  `id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `level` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_Useruserid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Dictionary`
--

INSERT INTO `Dictionary` (`id`, `language`, `theme`, `public`, `level`, `fk_Useruserid`) VALUES
(51, 'Anglų', 'Gyvūnai', 1, 'B2', '696771c817dc3c873dcbbcc8fe7697b2'),
(52, 'Anglų', 'Testas', 1, 'B2', '696771c817dc3c873dcbbcc8fe7697b2'),
(53, 'Anglų', 'Pradmenys', 1, 'A1', '696771c817dc3c873dcbbcc8fe7697b2'),
(54, 'Ispanų', 'Pradmenys', 1, 'A1', '696771c817dc3c873dcbbcc8fe7697b2');

-- --------------------------------------------------------

--
-- Table structure for table `DictionaryWord`
--

CREATE TABLE `DictionaryWord` (
  `id_DictionaryWord` int(11) NOT NULL,
  `fk_Wordid` int(11) NOT NULL,
  `fk_Dictionaryid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `DictionaryWord`
--

INSERT INTO `DictionaryWord` (`id_DictionaryWord`, `fk_Wordid`, `fk_Dictionaryid`) VALUES
(128, 150, 51),
(129, 151, 51),
(130, 152, 51),
(131, 153, 52),
(132, 154, 52),
(133, 155, 53),
(134, 156, 53),
(135, 157, 54),
(136, 158, 54);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userlevel` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `lastLanguage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastTheme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastLevel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`username`, `password`, `userid`, `userlevel`, `email`, `timestamp`, `lastLanguage`, `lastTheme`, `lastLevel`) VALUES
('krital2', '4625f6ab16a19cc9807c7c506ae18134', '34e96fdb57ce522638929b9758e4817c', 9, 'kristupas.talacka@ktu.edu', '2020-12-02 04:16:57', NULL, NULL, NULL),
('krital3', '4625f6ab16a19cc9807c7c506ae18134', '696771c817dc3c873dcbbcc8fe7697b2', 5, 'kristalacka@gmail.com', '2020-12-02 13:23:43', 'English', 'Animals', 'B2'),
('krital', '4625f6ab16a19cc9807c7c506ae18134', '8ca5bf589bfeddbdd33cc0beb55dc005', 4, 'kristalacka@gmail.com', '2020-12-02 13:41:27', 'Anglų', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Word`
--

CREATE TABLE `Word` (
  `id` int(11) NOT NULL,
  `word` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `translation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Word`
--

INSERT INTO `Word` (`id`, `word`, `translation`) VALUES
(150, 'dog', 'šuo'),
(151, 'cat', 'katė'),
(152, 'bird', 'paukštis'),
(153, 'test', 'testas'),
(154, 'testing', 'testavimas'),
(155, 'hello', 'labas'),
(156, 'name', 'vardas'),
(157, 'hola', 'labas'),
(158, 'nombre', 'vardas');

-- --------------------------------------------------------

--
-- Table structure for table `WordStats`
--

CREATE TABLE `WordStats` (
  `timesWrong` int(11) DEFAULT NULL,
  `learned` tinyint(1) DEFAULT NULL,
  `id_WordStats` int(11) NOT NULL,
  `fk_Useruserid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_Wordid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `WordStats`
--

INSERT INTO `WordStats` (`timesWrong`, `learned`, `id_WordStats`, `fk_Useruserid`, `fk_Wordid`) VALUES
(4, 0, 21, '8ca5bf589bfeddbdd33cc0beb55dc005', 150),
(0, 1, 22, '8ca5bf589bfeddbdd33cc0beb55dc005', 151),
(2, 1, 23, '8ca5bf589bfeddbdd33cc0beb55dc005', 152);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Dictionary`
--
ALTER TABLE `Dictionary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creates` (`fk_Useruserid`);

--
-- Indexes for table `DictionaryWord`
--
ALTER TABLE `DictionaryWord`
  ADD PRIMARY KEY (`id_DictionaryWord`),
  ADD KEY `belongs_to` (`fk_Wordid`),
  ADD KEY `has` (`fk_Dictionaryid`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `Word`
--
ALTER TABLE `Word`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `WordStats`
--
ALTER TABLE `WordStats`
  ADD PRIMARY KEY (`id_WordStats`),
  ADD KEY `has2` (`fk_Useruserid`),
  ADD KEY `belongs_to2` (`fk_Wordid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Dictionary`
--
ALTER TABLE `Dictionary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `DictionaryWord`
--
ALTER TABLE `DictionaryWord`
  MODIFY `id_DictionaryWord` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `Word`
--
ALTER TABLE `Word`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `WordStats`
--
ALTER TABLE `WordStats`
  MODIFY `id_WordStats` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Dictionary`
--
ALTER TABLE `Dictionary`
  ADD CONSTRAINT `creates` FOREIGN KEY (`fk_Useruserid`) REFERENCES `User` (`userid`);

--
-- Constraints for table `DictionaryWord`
--
ALTER TABLE `DictionaryWord`
  ADD CONSTRAINT `belongs_to` FOREIGN KEY (`fk_Wordid`) REFERENCES `Word` (`id`),
  ADD CONSTRAINT `has` FOREIGN KEY (`fk_Dictionaryid`) REFERENCES `Dictionary` (`id`);

--
-- Constraints for table `WordStats`
--
ALTER TABLE `WordStats`
  ADD CONSTRAINT `belongs_to2` FOREIGN KEY (`fk_Wordid`) REFERENCES `Word` (`id`),
  ADD CONSTRAINT `has2` FOREIGN KEY (`fk_Useruserid`) REFERENCES `User` (`userid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
