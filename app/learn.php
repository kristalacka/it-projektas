<!DOCTYPE html>

<html lang="en">

<style>
.msg {
    border: 1px solid #bbb;
    padding: 5px;
    margin: 10px 0px;
    background: #eee;
}
</style>

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>

<body>
    <center>
        <h1>Pirmasis mokymosi etapas</h1>
        <?php
        session_start();
        include("../include/nustatymai.php");
        ?>
        <p>Ką reiškia žodis <b><?php echo $_SESSION['words'][$_SESSION['current_word']]['word'] ?></b>?</p>
        <form method="post">
            <label for="translation">Atsakymas:</label>
            <input type="text" id="translation" name="translation" autocomplete="off"> <br />
            <input type="submit" name="continue" value="Tęsti" />
            <input type="submit" name="exit" value="Atšaukti" onclick="return confirm('Ar tikrai norite išeiti?');" />
        </form>

        <?php
        $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($db, 'utf8');
        $userid = $_SESSION['userid'];

        $lang = $_SESSION['lang'];
        $level = $_SESSION['level'];
        $theme = $_SESSION['theme'];
        $sql = "SELECT id as ID FROM Dictionary WHERE language=\"$lang\" AND theme=\"$theme\" AND level=\"$level\"";
        $result = mysqli_query($db, $sql);
        $row = mysqli_fetch_assoc($result);
        $dictid = $row['ID'];

        $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
        $result = mysqli_query($db, $sql);
        $full_count = mysqli_fetch_assoc($result)['cnt'];

        $sql = "SELECT count(*) as cnt FROM DictionaryWord"
            . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
            . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
            . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
        $result = mysqli_query($db, $sql);
        $learned_count = mysqli_fetch_assoc($result)['cnt'];
        if ($full_count > 0) {
            $progress = round($learned_count * 100 / $full_count, 2);
        } else {
            $progress = 0;
        }

        echo "Žodyno progresas: $progress%"
        ?>

        <?php
        function continue_fn()
        {
            $index = $_SESSION['current_word'];
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $wordid = $_SESSION['words'][$_SESSION['current_word']]['id'];
            $userid = $_SESSION['userid'];
            if (mb_strtolower($_POST['translation']) != mb_strtolower($_SESSION['words'][$index]['translation'])) {
                // push to incorrect words, update timeswrong
                array_push($_SESSION['incorrect_words'], $_SESSION['words'][$_SESSION['current_word']]);

                $sql = "UPDATE WordStats SET timesWrong = timesWrong + 1" .
                    " WHERE fk_Useruserid=\"$userid\" AND fk_Wordid=$wordid";
                mysqli_query($db, $sql);
            } else {
                // update wordstats learned
                $sql = "UPDATE WordStats SET learned = 1" .
                    " WHERE fk_Useruserid=\"$userid\" AND fk_Wordid=$wordid";
                mysqli_query($db, $sql);
            }
            if ($_SESSION['current_word'] < $_SESSION['total_words'] - 1) {
                $_SESSION['current_word']++;
                header('Location: learn.php');
            } else {
                // last word, move on to round 2, set total words to total remaining
                $_SESSION['total_words'] = count($_SESSION['incorrect_words']);
                $_SESSION['current_word'] = 0;
                header('Location: round2.php');
            }
            exit();
        }

        if (array_key_exists('continue', $_POST)) {
            continue_fn();
        }
        if (array_key_exists('exit', $_POST)) {
            unset($_SESSION['lang']);
            unset($_SESSION['level']);
            unset($_SESSION['theme']);
            unset($_SESSION['words']);
            unset($_SESSION['total_words']);
            unset($_SESSION['current_word']);
            unset($_SESSION['incorrect_words']);
            unset($_SESSION['incorrect_words2']);
            header('Location: /index.php');
            exit();
        }
        ?>
    </center>
</body>

</html>