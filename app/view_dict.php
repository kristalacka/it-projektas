<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>
<?php
session_start();
include("../include/nustatymai.php");
?>

<body>
    <a href="/">Atgal</a>
    <center>
        <h1>Žodyno peržiūra</h1>
        <?php
        $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($db, 'utf8');
        $lang = $_SESSION['lang'];
        $level = $_SESSION['level'];
        $theme = $_SESSION['theme'];
        $userid = $_SESSION['userid'];
        $sql = "SELECT id FROM Dictionary WHERE language=\"$lang\" AND level=\"$level\" AND theme=\"$theme\" AND (public = 1 OR fk_Useruserid=\"$userid\")";
        $result = mysqli_query($db, $sql);
        $row = mysqli_fetch_assoc($result);
        $dictid = $row['id'];

        $sql = "SELECT Word.word, Word.translation FROM Word LEFT JOIN DictionaryWord ON Word.id=DictionaryWord.fk_Wordid WHERE DictionaryWord.fk_Dictionaryid=$dictid";
        $result = mysqli_query($db, $sql);
        if (!$result || mysqli_num_rows($result) == 0) {
            echo "Įrašų nėra <br />";
        } else {
            echo "<table>";
            echo "<th>Žodis</th><th>Atsakymas</th>";
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                $word = $row['word'];
                $trans = $row['translation'];
                echo "<td>$word</td>";
                echo "<td>$trans</td>";
                echo "</tr>";
            }
            echo "</table>";
        }

        // echo "<table>";
        // echo "<th>Žodis</th><th>Atsakymas</th>";
        // foreach ($_SESSION['words'] as $wordw) {
        //     echo "<tr>";
        //     $word = $wordw['word'];
        //     $trans = $wordw['translation'];
        //     echo "<td>$word</td>";
        //     echo "<td>$trans</td>";
        //     echo "</tr>";
        // }
        // echo "</table>";
        ?>
    </center>
</body>

</html>