<?php
require('../tfpdf/tfpdf.php');
session_start();
$count = count($_SESSION['wordcards']);

$pdf = new tFPDF();
$pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
$pdf->SetFont('DejaVu', '', 14);
$pdf->SetMargins(0, 0);
$pdf->SetAutoPageBreak(false);

$i = 0;
foreach ($_SESSION['wordcards'] as $item) {
    if ($i % 5 == 0) {
        $pdf->AddPage();
    }
    $i++;
    $pdf->Cell($pdf->GetPageWidth() / 2, $pdf->GetPageHeight() / 5, $item['word'], 1, 0, 'C');
    $pdf->Cell($pdf->GetPageWidth() / 2, $pdf->GetPageHeight() / 5, $item['translation'], 1, 1, 'C');
    // $pdf->Cell($pdf->GetPageWidth() / 2, $pdf->GetPageHeight() / 5,  $pdf->GetPageHeight(), 1, 0);
}

$pdf->Output();