<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>

<body>
    <a href="/">Atgal</a>
    <center>
        <h1>Raportas</h1>
        <?php
        session_start();
        include("../include/nustatymai.php");
        $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($db, 'utf8');
        $userid = $_SESSION['userid'];
        $sql = "SELECT username FROM User WHERE User.userid=\"$userid\"";
        $result = mysqli_query($db, $sql);
        $row = mysqli_fetch_assoc($result);
        $name = $row['username'];
        echo "<p>Vartotojas: $name</p>";

        $sql_words = "SELECT Word.word, Word.translation, WordStats.timesWrong FROM Word" .
            " LEFT JOIN WordStats on Word.id=WordStats.fk_Wordid" .
            " LEFT JOIN User on User.userid=WordStats.fk_Useruserid" .
            " WHERE User.userid=\"$userid\" AND WordStats.timesWrong > 0 ORDER BY WordStats.timesWrong DESC";

        if (mysqli_num_rows($result) == 0) {
            echo "<p>Vartotojas dar nėra suklydęs.</p>";
        } else {
            echo "<p>Sunkiausių žodžių sąrašas: </p><br />";
            $result = mysqli_query($db, $sql_words);
            echo "<table>";
            echo "<tr style=\"padding: 5px;\"><th style=\"padding: 5px;\">Žodis</th><th style=\"padding: 5px;\">Reikšmė</th><th style=\"padding: 5px;\">Neteisingai atsakyta</th></tr>";
            $_SESSION['wordcards'] = [];
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($_SESSION['wordcards'], $row);
                $word = $row['word'];
                $translation = $row['translation'];
                $count = $row['timesWrong'];
                echo "<tr style=\"padding: 5px;\">";
                echo "<td style=\"padding: 5px;\">$word</td><td style=\"padding: 5px;\">$translation</td><td style=\"padding: 5px;\">$count</td>";
                echo "</tr>";
            }
            echo "</table>";
            echo "<form target=\"_blank\" action=\"make_pdf.php\" method=\"post\">";
            echo "<button type=\"submit\" name=\"button1\">Parsisiųsti mokymosi korteles</button>";
            echo "</form>";
        }
        ?>
    </center>
</body>

</html>