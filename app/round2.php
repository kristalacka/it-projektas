<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>

<?php
session_start();
if (count($_SESSION['incorrect_words']) == 0) {
    header('Location: results.php');
    exit;
}
include("../include/nustatymai.php");
?>

<body>
    <center>
        <h1>Antrasis mokymosi etapas</h1>
        <p>Šie žodžiai buvo įvesti klaidingai, bandykite dar kartą.</p>
        <p>Ką reiškia žodis <b><?php echo $_SESSION['incorrect_words'][$_SESSION['current_word']]['word'] ?></b>?</p>
        <form method="post">
            <label for="translation">Atsakymas:</label>
            <input type="text" id="translation" name="translation" autocomplete="off"> <br />
            <input type="submit" name="continue" value="Tęsti" />
            <input type="submit" name="exit" value="Atšaukti" onclick="return confirm('Ar tikrai norite išeiti?');" />
        </form>

        <?php
        $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($db, 'utf8');
        $userid = $_SESSION['userid'];

        $lang = $_SESSION['lang'];
        $level = $_SESSION['level'];
        $theme = $_SESSION['theme'];
        $sql = "SELECT id FROM Dictionary WHERE language=\"$lang\" AND theme=\"$theme\" AND level=\"$level\"";
        $result = mysqli_query($db, $sql);
        $dictid = mysqli_fetch_assoc($result)['id'];

        $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
        $result = mysqli_query($db, $sql);
        $full_count = mysqli_fetch_assoc($result)['cnt'];

        $sql = "SELECT count(*) as cnt FROM DictionaryWord"
            . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
            . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
            . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
        $result = mysqli_query($db, $sql);
        $learned_count = mysqli_fetch_assoc($result)['cnt'];

        $progress = round($learned_count * 100 / $full_count, 2);
        echo "Žodyno progresas: $progress%";
        ?>

        <?php
        function continue_fn()
        {
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            $userid = $_SESSION['userid'];
            $index = $_SESSION['current_word'];
            $wordid = $_SESSION['incorrect_words'][$index]['id'];
            // word guessed incorrectly
            if (mb_strtolower($_POST['translation']) != mb_strtolower($_SESSION['incorrect_words'][$index]['translation'])) {
                array_push($_SESSION['incorrect_words2'], $_SESSION['incorrect_words'][$index]);
                // push to incorrect words 2, update timeswrong

                $sql = "UPDATE WordStats SET timesWrong = timesWrong + 1" .
                    " WHERE fk_Useruserid=\"$userid\" AND fk_Wordid=$wordid";
                mysqli_query($db, $sql);
            } else {
                // update wordstats learned
                $sql = "UPDATE WordStats SET learned = 1" .
                    " WHERE fk_Useruserid=\"$userid\" AND fk_Wordid=$wordid";
                mysqli_query($db, $sql);
            }
            if ($_SESSION['current_word'] < $_SESSION['total_words'] - 1) {
                $_SESSION['current_word']++;
                header('Location: round2.php');
                exit;
            } else {
                // last word, finished
                if (count($_SESSION['incorrect_words2']) > 0) {
                    $_SESSION['incorrect_words'] = $_SESSION['incorrect_words2'];
                    shuffle($_SESSION['incorrect_words']);
                    $_SESSION['incorrect_words2'] = array();
                    $_SESSION['current_word'] = 0;
                    $_SESSION['total_words'] = count($_SESSION['incorrect_words']);
                    header('Location: round2.php');
                    exit;
                } else {
                    header('Location: results.php');
                    exit;
                }
            }
            exit;
        }

        if (array_key_exists('continue', $_POST)) {
            continue_fn();
        }
        if (array_key_exists('exit', $_POST)) {
            unset($_SESSION['lang']);
            unset($_SESSION['level']);
            unset($_SESSION['theme']);
            unset($_SESSION['words']);
            unset($_SESSION['total_words']);
            unset($_SESSION['current_word']);
            unset($_SESSION['incorrect_words']);
            unset($_SESSION['incorrect_words2']);
            header('Location: /index.php');
            exit();
        }
        ?>
    </center>
</body>

</html>