<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>

<?php
session_start();
include("../include/nustatymai.php");
function startsWith($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}
?>

<body>
    <!-- <a href="/">Atgal</a> -->
    <form method="post">
        <input type="submit" id="back" name="back" value="Atgal">
    </form>
    <center>
        <h1>Žodyno pridėjimas</h1>
        <form method="post">
            <table>
                <tr>
                    <td>Kalba:</td>
                    <td><input type="text" id="language" name="language" <?php
                                                                            if (isset($_SESSION['add_language']) && !empty($_SESSION['add_language'])) {
                                                                                $language = $_SESSION['add_language'];
                                                                                echo "value=\"$language\"";
                                                                            }
                                                                            ?>></td>
                </tr>
                <tr>
                    <td>Lygis:</td>
                    <td><select name="level" id="level">
                            <option value="default" <?php
                                                    if (!isset($_SESSION['add_level'])) {
                                                        echo " selected";
                                                    }
                                                    ?>>Pasirinkite lygį</option>
                            <option value="A1" <?php
                                                if (isset($_SESSION['add_level']) && $_SESSION['add_level'] == "A1") {
                                                    echo " selected";
                                                }
                                                ?>>A1</option>
                            <option value="A2" <?php
                                                if (isset($_SESSION['add_level']) && $_SESSION['add_level'] == "A2") {
                                                    echo " selected";
                                                }
                                                ?>>A2</option>
                            <option value="B1" <?php
                                                if (isset($_SESSION['add_level']) && $_SESSION['add_level'] == "B1") {
                                                    echo " selected";
                                                }
                                                ?>>B1</option>
                            <option value="B2" <?php
                                                if (isset($_SESSION['add_level']) && $_SESSION['add_level'] == "B2") {
                                                    echo " selected";
                                                }
                                                ?>>B2</option>
                            <option value="C1" <?php
                                                if (isset($_SESSION['add_level']) && $_SESSION['add_level'] == "C1") {
                                                    echo " selected";
                                                }
                                                ?>>C1</option>
                            <option value="C2" <?php
                                                if (isset($_SESSION['add_level']) && $_SESSION['add_level'] == "C2") {
                                                    echo " selected";
                                                }
                                                ?>>C2</option>
                        </select></td>
                </tr>
                <tr>
                    <td>Tema:</td>
                    <td><input type="text" id="theme" name="theme" <?php
                                                                    if (isset($_SESSION['add_theme']) && !empty($_SESSION['add_theme'])) {
                                                                        $theme = $_SESSION['add_theme'];
                                                                        echo "value=\"$theme\"";
                                                                    }
                                                                    ?>></td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <th>Žodis</th>
                    <th>Reikšmė</th>
                    <th>Funkcija</th>
                </tr>
                <?php
                if (isset($_SESSION['words_to_insert'])) {
                    $i = 0;
                    foreach ($_SESSION['words_to_insert'] as $item) {
                        $word = $item['word'];
                        $translation = $item['translation'];
                        echo "<tr>";
                        echo "<td>$word</td>";
                        echo "<td>$translation</td>";
                        echo "<td><input type=\"submit\" id=\"remove\" name=\"action\" value=\"Šalinti $i\"></td>";
                        echo "</tr>";
                        $i++;
                    }
                }
                ?>
                <tr>
                    <td><input type="text" id="word" name="word"></td>
                    <td><input type="text" id="translation" name="translation"></td>
                    <td><input type="submit" id="add" name="action" value="Pridėti"></td>
                </tr>
            </table>
            <br />
            <input type="submit" id="finish" name="action" value="Baigti">
        </form>

        <?php
        if (array_key_exists('back', $_POST)) {
            unset($_SESSION['words_to_insert']);
            unset($_SESSION['add_language']);
            unset($_SESSION['add_level']);
            unset($_SESSION['add_theme']);
            header('Location: /');
            exit();
        }
        if (array_key_exists('action', $_POST)) {
            if ($_POST['action'] == 'Baigti') {
                $_SESSION['add_language'] = $_POST['language'];
                $_SESSION['add_level'] = $_POST['level'];
                $_SESSION['add_theme'] = $_POST['theme'];
                if (isset($_SESSION['add_language']) && isset($_SESSION['add_level']) && isset($_SESSION['add_theme']) && $_SESSION['add_level'] != "default") {
                    $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                    mysqli_set_charset($db, 'utf8');
                    $userid = $_SESSION['userid'];
                    $language = $_SESSION['add_language'];
                    $level = $_SESSION['add_level'];
                    $theme = $_SESSION['add_theme'];
                    $dict_id = -1;
                    $err = false;
                    if (strlen($language) > 0 && strlen($theme) > 0 && strlen($level) == 2) {
                        $sql = "INSERT INTO Dictionary (language, theme, level, fk_Useruserid) VALUES (\"$language\", \"$theme\", \"$level\", \"$userid\")";
                        mysqli_query($db, $sql);

                        $sql_dictid = "SELECT max(id) FROM Dictionary";
                        $result = mysqli_query($db, $sql_dictid);
                        $row = mysqli_fetch_assoc($result);
                        $dict_id = $row['max(id)'];

                        foreach ($_SESSION['words_to_insert'] as $item) {
                            $word = $item['word'];
                            $translation = $item['translation'];
                            $sql_check = "SELECT count(*) as cnt FROM Word LEFT JOIN DictionaryWord ON Word.id = DictionaryWord.fk_Wordid WHERE"
                                . " Word.word=\"$word\" AND DictionaryWord.fk_Dictionaryid=$dict_id";
                            $result = mysqli_query($db, $sql_check);
                            $row = mysqli_fetch_assoc($result);
                            if ($row['cnt'] == 0) {
                                $sql_word = "INSERT INTO Word (word, translation) VALUES (\"$word\", \"$translation\")";
                                $result = mysqli_query($db, $sql_word);
                                mysqli_fetch_assoc($result);
                                $sql_id = "SELECT max(id) FROM Word";
                                $result = mysqli_query($db, $sql_id);
                                $row = mysqli_fetch_assoc($result);
                                $word_id = $row['max(id)'];

                                $sql_dictword = "INSERT INTO DictionaryWord (fk_Dictionaryid, fk_Wordid) VALUES ($dict_id, $word_id)";
                                mysqli_query($db, $sql_dictword);
                            } else {
                                echo "Klaida bandant įterpti žodį $word <br/>";
                                $err = true;
                                break;
                            }
                        }
                        if ($err) {
                            $sql = "SELECT fk_Wordid FROM DictionaryWord WHERE fk_Dictionaryid=$dict_id";
                            $result = mysqli_query($db, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                $wordid = $row['fk_Wordid'];
                                $sqldel = "DELETE FROM Word WHERE id=$wordid";
                                mysqli_query($db, $sqldel);
                            }
                            $sql = "DELETE FROM DictionaryWord WHERE fk_Dictionaryid=$dict_id";
                            $result = mysqli_query($db, $sql);
                            mysqli_fetch_assoc($result);
                            $sql = "DELETE FROM Dictionary WHERE id=$dict_id";
                            $result = mysqli_query($db, $sql);
                            mysqli_fetch_assoc($result);
                            header('Location: ' . $_SERVER['PHP_SELF']);
                            exit();
                        } else {
                            unset($_SESSION['words_to_insert']);
                            unset($_SESSION['add_language']);
                            unset($_SESSION['add_level']);
                            unset($_SESSION['add_theme']);
                            header('Location: /');
                            exit();
                        }
                    } else {
                        echo "Klaida žodyno parametruose <br/>";
                        header('Location: ' . $_SERVER['PHP_SELF']);
                        exit();
                    }
                } else {
                    echo "Klaida žodyno parametruose";
                }
            } else if ($_POST['action'] == 'Pridėti') {
                $_SESSION['add_language'] = $_POST['language'];
                $_SESSION['add_level'] = $_POST['level'];
                $_SESSION['add_theme'] = $_POST['theme'];
                if (isset($_POST['word']) && isset($_POST['translation']) && !empty($_POST['word']) && !empty($_POST['translation'])) {
                    if (!isset($_SESSION['words_to_insert'])) {
                        $_SESSION['words_to_insert'] = [];
                    }
                    array_push($_SESSION['words_to_insert'], array('word' => $_POST['word'], 'translation' => $_POST['translation']));
                    echo substr($_SESSION['add_level'], 0, 2);
                    header('Location: ' . $_SERVER['PHP_SELF']);
                    exit();
                }
            } else if (startsWith($_POST['action'], "Šalinti")) {
                $number = intval(substr($_POST['action'], 9));
                unset($_SESSION['words_to_insert'][$number]);
                $_SESSION['words_to_insert'] = array_values($_SESSION['words_to_insert']);
                header('Location: ' . $_SERVER['PHP_SELF']);
                exit();
            }
        }
        ?>
    </center>
</body>

</html>