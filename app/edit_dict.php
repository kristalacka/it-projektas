<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>


<?php
session_start();
include("../include/nustatymai.php");
function startsWith($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}
?>

<body>
    <form method="post">
        <input type="submit" id="back" name="back" value="Atgal"><br />
        <input type="submit" id="home" name="home" value="Pagrindinis"> <br />
    </form>
</body>

<?php
if (isset($_GET['id'])) {
    makeTable($_GET['id']);
}

function makeTable($id)
{
    echo "<center>";
    echo "<form method=\"post\">";
    $lang = "";
    $level = "";
    $theme = "";
    if (!isset($_SESSION['editwords'])) {
        $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($db, 'utf8');
        $sql = "SELECT * FROM Dictionary WHERE id=$id";
        $result = mysqli_query($db, $sql);
        $row = mysqli_fetch_assoc($result);
        $lang = $row['language'];
        $level = $row['level'];
        $theme = $row['theme'];
        $_SESSION['editlang'] = $lang;
        $_SESSION['editlevel'] = $level;
        $_SESSION['edittheme'] = $theme;
    } else {
        $lang = $_SESSION['editlang'];
        $level = $_SESSION['editlevel'];
        $theme = $_SESSION['edittheme'];
    }
    echo "<table>";
    echo "<tr>";
    echo "<td>ID:</td>";
    echo "<td><input type=\"text\" id=\"dictid\" name=\"dictid\" value=$id readonly></td>";
    echo "</tr><tr>";
    echo "<tr>";
    echo "<td>Kalba:</td>";
    echo "<td><input type=\"text\" id=\"language\" name=\"language\" value=$lang></td>";
    echo "</tr><tr>";
    echo "<td>Lygis:</td>";
    echo "<td><select name=\"level\" id=\"level\">";
    echo "<option value=\"A1\"";
    if ($level == "A1") {
        echo " selected";
    }
    echo ">A1</option>";
    echo "<option value=\"A2\"";
    if ($level == "A2") {
        echo " selected";
    }
    echo ">A2</option>";
    echo "<option value=\"B1\"";
    if ($level == "B1") {
        echo " selected";
    }
    echo ">B1</option>";
    echo "<option value=\"B2\"";
    if ($level == "B2") {
        echo " selected";
    }
    echo ">B2</option>";
    echo "<option value=\"C1\"";
    if ($level == "C1") {
        echo " selected";
    }
    echo ">C1</option>";
    echo "<option value=\"C2\"";
    if ($level == "C2") {
        echo " selected";
    }
    echo ">C2</option>";
    echo "</td></select>";
    echo "</tr><tr>";
    echo "<td>Tema:</td>";
    echo "<td><input type=\"text\" id=\"theme\" name=\"theme\" value=$theme></td>";
    echo "</tr>";
    echo "</table>";
    echo "<table><tr><th>Žodis</th><th>Reikšmė</th><th>Funkcija</th></tr>";
    if (!isset($_SESSION['editwords'])) {
        $_SESSION['editwords'] = [];
        $sql = "SELECT Word.id, Word.word, Word.translation FROM DictionaryWord LEFT JOIN Word ON Word.id = DictionaryWord.fk_Wordid WHERE fk_Dictionaryid=$id";
        $result = mysqli_query($db, $sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $word = $row['word'];
            $translation = $row['translation'];
            array_push($_SESSION['editwords'], array('word' => $row['word'], 'translation' => $row['translation']));
        }
    }
    $i = 0;
    foreach ($_SESSION['editwords'] as $row) {
        $word = $row['word'];
        $translation = $row['translation'];
        array_push($_SESSION['editwords'], array('word' => $row['word'], 'translation' => $row['translation']));
        echo "<tr>";
        echo "<td><input type=\"text\" id = \"words[$i]\" name=\"words[$i]\" value=\"$word\"></td>";
        echo "<td><input type=\"text\" id = \"translations[$i]\" name=\"translations[$i]\" value=\"$translation\"></td>";
        echo "<td><input type=\"submit\" id=\"remove\" name=\"action\" value=\"Šalinti $i\"></td>";
        echo "</tr>";
        $i++;
    }

    echo "<tr>";
    echo "<td><input type=\"text\" id=\"word\" name=\"word\"></td>";
    echo "<td><input type=\"text\" id=\"translation\" name=\"translation\"></td>";
    echo "<td><input type=\"submit\" id=\"add\" name=\"action\" value=\"Pridėti\"></td>";
    echo "</tr>";
    echo "</table>";
    echo "<input type=\"submit\" id=\"finish\" name=\"action\" value=\"Baigti\">";
    echo "</form>";
    echo "</center>";
}

if (array_key_exists('back', $_POST)) {
    unset($_SESSION['editwords']);
    unset($_SESSION['editlang']);
    unset($_SESSION['editlevel']);
    unset($_SESSION['edittheme']);
    $userlevel = $_SESSION['ulevel'];
    $role = ""; {
        foreach ($user_roles as $x => $x_value) {
            if ($x_value == $userlevel) $role = $x;
        }
    }
    if ($userlevel == $user_roles[CREATOR_LEVEL]) {
        header('Location: /app/creator_dict_list.php');
    } else {
        header('Location: /app/dict_list.php');
    }

    exit();
}
if (array_key_exists('home', $_POST)) {
    unset($_SESSION['editwords']);
    unset($_SESSION['editlang']);
    unset($_SESSION['editlevel']);
    unset($_SESSION['edittheme']);
    header('Location: /');
    exit();
}
if (array_key_exists('action', $_POST)) {
    if ($_POST['action'] == 'Baigti') {
        unset($_SESSION['editwords']);
        $_SESSION['editwords'] = [];
        if (isset($_POST['words'])) {
            for ($i = 0; $i < count($_POST['words']); $i++) {
                array_push($_SESSION['editwords'], array('word' => $_POST['words'][$i], 'translation' =>  $_POST['translations'][$i]));
            }
        }

        $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        mysqli_set_charset($db, 'utf8');
        $userid = $_SESSION['userid'];
        // $language = $_SESSION['editlang'];
        // $level = $_SESSION['editlevel'];
        // $theme = $_SESSION['edittheme'];
        $language = $_POST['language'];
        $level = $_POST['level'];
        $theme = $_POST['theme'];
        $dictid = $_POST['dictid'];
        if (strlen($language) > 0 && strlen($theme) > 0) {
            $sql = "UPDATE Dictionary SET level=\"$level\", theme=\"$theme\", language=\"$language\" WHERE id=$dictid";
            $result = mysqli_query($db, $sql);
            mysqli_fetch_assoc($result);
            $sql = "SELECT fk_Wordid FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
            $result = mysqli_query($db, $sql);
            $to_delete = [];
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($to_delete, $row['fk_Wordid']);
            }
            $sql = "DELETE FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
            $result = mysqli_query($db, $sql);
            mysqli_fetch_assoc($result);
            foreach ($to_delete as $wordid) {
                $sqldel = "DELETE FROM Word WHERE id=$wordid";
                $resultd = mysqli_query($db, $sqldel);
                mysqli_fetch_assoc($resultd);
            }
            foreach ($_SESSION['editwords'] as $item) {
                $word = $item['word'];
                $translation = $item['translation'];
                $sql_word = "INSERT INTO Word (word, translation) VALUES (\"$word\", \"$translation\")";
                $result = mysqli_query($db, $sql_word);
                $sql_id = "SELECT max(id) FROM Word";
                $result = mysqli_query($db, $sql_id);
                $row = mysqli_fetch_assoc($result);
                $word_id = $row['max(id)'];

                $sql_dictword = "INSERT INTO DictionaryWord (fk_Dictionaryid, fk_Wordid) VALUES ($dictid, $word_id)";
                $resulti = mysqli_query($db, $sql_dictword);
                mysqli_fetch_assoc($resulti);
            }

            unset($_SESSION['editwords']);
            echo "<script>window.location.href='/app/dict_list.php'</script>";
            exit();
        } else {
            echo "Žodyno parametrai negali būti tušti <br/>";
            echo '<script language="javascript">alert("Žodyno parametrai negali būti tušti")</script>';
            echo "<script>window.location.href='/app/edit_dict.php?id=$dictid'</script>";
            exit();
        }
    } else if ($_POST['action'] == 'Pridėti') {
        unset($_SESSION['editwords']);
        $_SESSION['editwords'] = [];
        for ($i = 0; $i < count($_POST['words']); $i++) {
            array_push($_SESSION['editwords'], array('word' => $_POST['words'][$i], 'translation' =>  $_POST['translations'][$i]));
        }
        $dictid = $_POST['dictid'];
        if (isset($_POST['word']) && isset($_POST['translation']) && !empty($_POST['word']) && !empty($_POST['translation'])) {
            if (!isset($_SESSION['editwords'])) {
                $_SESSION['editwords'] = [];
            }
            array_push($_SESSION['editwords'], array('word' => $_POST['word'], 'translation' => $_POST['translation']));
            echo "<script>window.location.href='/app/edit_dict.php?id=$dictid'</script>";
            exit();
        } else {
            echo "<script>window.location.href='/app/edit_dict.php?id=$dictid'</script>";
            exit();
        }
    } else if (startsWith($_POST['action'], "Šalinti")) {
        unset($_SESSION['editwords']);
        $_SESSION['editwords'] = [];
        for ($i = 0; $i < count($_POST['words']); $i++) {
            array_push($_SESSION['editwords'], array('word' => $_POST['words'][$i], 'translation' =>  $_POST['translations'][$i]));
        }
        $dictid = $_POST['dictid'];
        $number = intval(substr($_POST['action'], 9));
        unset($_SESSION['editwords'][$number]);
        $_SESSION['editwords'] = array_values($_SESSION['editwords']);
        header('Location: /app/edit_dict.php?id=' . $dictid);
        exit();
    }
}
?>