<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>


<?php
session_start();
include("../include/nustatymai.php");
function startsWith($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}
?>

<body>
    <a href="/">Atgal</a>
    <center>
        <h1>Visi žodynai</h1>
        <form method="post">
            <?php
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $userid = $_SESSION['userid'];
            $sql = "SELECT * FROM Dictionary";
            $result = mysqli_query($db, $sql);
            echo "<table><tr><th>Kalba</th><th>Lygmuo</th><th>Tema</th><th>Viešas</th><th>Redaguoti</th><th>Šalinti</th></tr>";
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr><td>" . $row['language'] . "</td><td>" . $row['level'] . "</td><td>" . $row['theme'] . "</td>";
                if ($row['public'] == 1) {
                    echo "<td>Taip</td>";
                } else {
                    echo "<td>Ne</td>";
                }
                $i = $row['id'];
                echo "<td><input type=\"submit\" id=\"edit\" name=\"action\" value=\"Redaguoti id $i\"></td>";
                echo "<td><input type=\"submit\" id=\"remove\" name=\"action\" value=\"Šalinti id $i\"></td>";
                if ($row['public'] == 0) {
                    echo "<td><input type=\"submit\" id=\"setpublic\" name=\"action\" value=\"Paviešinti id $i\"></td>";
                } else {
                    echo "<td><input type=\"submit\" id=\"setprivate\" name=\"action\" value=\"Slėpti id $i\"></td>";
                }
                echo "</tr>";
            }
            echo "</table>"
            ?>
        </form>
    </center>

    <?php
    if (array_key_exists('action', $_POST)) {
        if (startsWith($_POST['action'], "Redaguoti")) {
            $number = intval(substr($_POST['action'], 13));
            $_SESSION['editid'] = $number;
            header("Location: /app/edit_dict.php?id=$number");
            exit();
        } else if (startsWith($_POST['action'], "Šalinti")) {
            $number = intval(substr($_POST['action'], 12));
            if (startsWith($_POST['action'], "Redaguoti")) {
                $number = intval(substr($_POST['action'], 13));
                $_SESSION['editid'] = $number;
                header("Location: /app/edit_dict.php?id=$number");
                exit();
            } else if (startsWith($_POST['action'], "Šalinti")) {
                $number = intval(substr($_POST['action'], 12));
                $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                mysqli_set_charset($db, 'utf8');
                $sql = "SELECT fk_Wordid FROM DictionaryWord WHERE fk_Dictionaryid=$number";
                $result = mysqli_query($db, $sql);
                $toremove = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    array_push($toremove, $row['fk_Wordid']);
                }
                $sql = "DELETE FROM DictionaryWord WHERE fk_Dictionaryid=$number";
                $result = mysqli_query($db, $sql);
                mysqli_fetch_assoc($result);

                foreach ($toremove as $item) {
                    $sql = "DELETE FROM WordStats WHERE fk_Wordid=$item";
                    $result = mysqli_query($db, $sql);
                    mysqli_fetch_assoc($result);
                    $sql = "DELETE FROM Word WHERE id=$item";
                    $result = mysqli_query($db, $sql);
                    mysqli_fetch_assoc($result);
                }
                $sql = "DELETE FROM Dictionary WHERE id=$number";
                $result = mysqli_query($db, $sql);
                mysqli_fetch_assoc($result);
                header('Location: ' . $_SERVER['PHP_SELF']);
                exit();
            }
        } else if (startsWith($_POST['action'], "Paviešinti")) {
            $number = intval(substr($_POST['action'], 15));
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $sql = "UPDATE Dictionary SET public=1 WHERE id=$number";
            $result = mysqli_query($db, $sql);
            mysqli_fetch_assoc($result);
            header('Location: ' . $_SERVER['PHP_SELF']);
            exit();
        } else if (startsWith($_POST['action'], "Slėpti")) {
            $number = intval(substr($_POST['action'], 11));
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $sql = "UPDATE Dictionary SET public=0 WHERE id=$number";
            $result = mysqli_query($db, $sql);
            mysqli_fetch_assoc($result);
            header('Location: ' . $_SERVER['PHP_SELF']);
            exit();
        }
    }
    ?>
</body>