<!DOCTYPE html>

<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>

<style>
hr.rounded {
    border-top: 3px solid #bbb;
    border-radius: 3px;
}
</style>

<?php
$db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($db, 'utf8');
$user = $_SESSION['userid'];
$sql = "SELECT lastLanguage, lastTheme, lastLevel FROM User WHERE userid = \"$user\"";
$result = mysqli_query($db, $sql);
$row = mysqli_fetch_assoc($result);
if ($row['lastLanguage'] != NULL) {
    $_SESSION['lang'] = $row['lastLanguage'];
    if ($row['lastLevel'] != NULL) {
        $_SESSION['level'] = $row['lastLevel'];
        if ($row['lastTheme'] != NULL) {
            $_SESSION['theme'] = $row['lastTheme'];
        }
    }
}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
?>

<body>
    <hr class="rounded">
    <center>

        <table>
            <tr style="padding: 10px;">
                <td style="padding: 5px;">
                    <label for="lang">Pasirinkite kalbą:</label>
                </td>
                <form method="post">
                    <td style="padding: 5px;">
                        <select name="lang" id="lang" <?php if (isset($_SESSION['lang'])) {
                                                            echo "disabled";
                                                        } ?>>
                            <option value="default">Pasirinkte kalbą</option>
                            <?php
                            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                            mysqli_set_charset($db, 'utf8');
                            $userid = $_SESSION['userid'];
                            $sql = "SELECT DISTINCT language FROM Dictionary WHERE public=1 OR fk_Useruserid=\"$userid\"";
                            $result = mysqli_query($db, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                $lang = $row['language'];
                                if (isset($_SESSION['lang']) && $_SESSION['lang'] == $lang) {
                                    echo "<option value=\"$lang\" selected>$lang</option>";
                                } else {
                                    echo "<option value=\"$lang\">$lang</option>";
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td style="padding: 5px;">
                        <input type="submit" name="chooselang" id="chooselang" value="Pasirinkti" <?php if (isset($_SESSION['lang'])) {
                                                                                                        echo "disabled";
                                                                                                    } ?> />
                    </td>
                    <td>
                        <?php
                        if (isset($_SESSION['lang'])) {
                            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                            mysqli_set_charset($db, 'utf8');
                            $userid = $_SESSION['userid'];

                            $lang = $_SESSION['lang'];
                            $sql = "SELECT id FROM Dictionary WHERE language=\"$lang\" AND (public=1 OR fk_Useruserid=\"$userid\")";
                            $result = mysqli_query($db, $sql);
                            $full_count = 0;
                            $learned_count = 0;
                            while ($row = mysqli_fetch_assoc($result)) {
                                $dictid = $row['id'];
                                $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
                                $result_a = mysqli_query($db, $sql);
                                $full_count += mysqli_fetch_assoc($result_a)['cnt'];

                                $sql = "SELECT count(*) as cnt FROM DictionaryWord"
                                    . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
                                    . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
                                    . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
                                $result_a = mysqli_query($db, $sql);
                                $learned_count += mysqli_fetch_assoc($result_a)['cnt'];
                            }

                            if ($full_count > 0) {
                                $progress = round($learned_count * 100 / $full_count, 2);
                                echo "Kalbos progresas: $progress%";
                            } else {
                                echo "Klaida: nėra įrašų";
                            }
                        }
                        ?>
                    </td>
                </form>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 5px;">
                    <label for="level">Pasirinkite mokymosi lygmenį:</label>
                </td>
                <form method="post">
                    <td style="padding: 5px;">
                        <select name="level" id="level" <?php if (
                                                            isset($_SESSION['lang']) && isset($_SESSION['level']) ||
                                                            !isset($_SESSION['lang']) && !isset($_SESSION['level'])
                                                        ) {
                                                            echo "disabled";
                                                        } ?>>
                            <option value="default" selected>Pasirinkte lygį</option>
                            <?php
                            if (isset($_SESSION['lang'])) {
                                $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                                mysqli_set_charset($db, 'utf8');
                                $lang = $_SESSION['lang'];
                                $userid = $_SESSION['userid'];
                                $sql = "SELECT DISTINCT level FROM Dictionary WHERE language=\"$lang\" AND (public=1 OR fk_Useruserid=\"$userid\")";
                                $result = mysqli_query($db, $sql);
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $level = $row['level'];
                                    if (isset($_SESSION['level']) && $_SESSION['level'] == $level) {
                                        echo "<option value=\"$level\" selected>$level</option>";
                                    } else {
                                        echo "<option value=\"$level\">$level</option>";
                                    }
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td style="padding: 5px;">
                        <input type="submit" name="chooselevel" id="chooselevel" value="Pasirinkti" <?php if (
                                                                                                        isset($_SESSION['lang']) && isset($_SESSION['level']) ||
                                                                                                        !isset($_SESSION['lang']) && !isset($_SESSION['level'])
                                                                                                    ) {
                                                                                                        echo "disabled";
                                                                                                    } ?> />
                    </td>
                    <td>
                        <?php
                        if (isset($_SESSION['level'])) {
                            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                            mysqli_set_charset($db, 'utf8');
                            $userid = $_SESSION['userid'];

                            $lang = $_SESSION['lang'];
                            $level = $_SESSION['level'];
                            $sql = "SELECT id FROM Dictionary WHERE language=\"$lang\" AND level=\"$level\" AND (public=1 OR fk_Useruserid=\"$userid\")";
                            $result = mysqli_query($db, $sql);
                            $full_count = 0;
                            $learned_count = 0;
                            while ($row = mysqli_fetch_assoc($result)) {
                                $dictid = $row['id'];
                                $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
                                $result_a = mysqli_query($db, $sql);
                                $full_count += mysqli_fetch_assoc($result_a)['cnt'];

                                $sql = "SELECT count(*) as cnt FROM DictionaryWord"
                                    . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
                                    . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
                                    . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
                                $result_a = mysqli_query($db, $sql);
                                $learned_count += mysqli_fetch_assoc($result_a)['cnt'];
                            }

                            if ($full_count > 0) {
                                $progress = round($learned_count * 100 / $full_count, 2);
                                echo "Lygio progresas: $progress%";
                            } else {
                                echo "Klaida, nėra įrašų";
                            }
                        }
                        ?>
                    </td>
                </form>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 5px;">
                    <label for="theme">Pasirinkite tematiką:</label>
                </td>
                <form method="post">
                    <td style="padding: 5px;">
                        <select name="theme" id="theme" <?php if (
                                                            isset($_SESSION['level']) && isset($_SESSION['theme']) ||
                                                            !isset($_SESSION['level']) && !isset($_SESSION['theme'])
                                                        ) {
                                                            echo "disabled";
                                                        } ?>>
                            <option value="default" selected>Pasirinkte temą</option>
                            <?php
                            if (isset($_SESSION['lang'])) {
                                $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                                mysqli_set_charset($db, 'utf8');
                                $lang = $_SESSION['lang'];
                                $level = $_SESSION['level'];
                                $userid = $_SESSION['userid'];
                                $sql = "SELECT DISTINCT theme FROM Dictionary WHERE language=\"$lang\" AND level=\"$level\" AND (public=1 OR fk_Useruserid=\"$userid\")";
                                $result = mysqli_query($db, $sql);
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $theme = $row['theme'];
                                    if (isset($_SESSION['theme']) && $_SESSION['theme'] == $theme) {
                                        echo "<option value=\"$theme\" selected>$theme</option>";
                                    } else {
                                        echo "<option value=\"$theme\">$theme</option>";
                                    }
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td style="padding: 5px;">
                        <input type="submit" name="choosetheme" id="choosetheme" value="Pasirinkti" <?php if (
                                                                                                        isset($_SESSION['level']) && isset($_SESSION['theme']) ||
                                                                                                        !isset($_SESSION['level']) && !isset($_SESSION['theme'])
                                                                                                    ) {
                                                                                                        echo "disabled";
                                                                                                    } ?> />
                    </td>
                    <td>
                        <?php
                        if (isset($_SESSION['theme'])) {
                            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                            mysqli_set_charset($db, 'utf8');
                            $userid = $_SESSION['userid'];

                            $lang = $_SESSION['lang'];
                            $level = $_SESSION['level'];
                            $theme = $_SESSION['theme'];
                            $sql = "SELECT id FROM Dictionary WHERE language=\"$lang\" AND level=\"$level\" AND theme=\"$theme\" AND (public=1 OR fk_Useruserid=\"$userid\")";
                            $result = mysqli_query($db, $sql);
                            $full_count = 0;
                            $learned_count = 0;
                            while ($row = mysqli_fetch_assoc($result)) {
                                $dictid = $row['id'];
                                $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
                                $result_a = mysqli_query($db, $sql);
                                $full_count += mysqli_fetch_assoc($result_a)['cnt'];

                                $sql = "SELECT count(*) as cnt FROM DictionaryWord"
                                    . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
                                    . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
                                    . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
                                $result_a = mysqli_query($db, $sql);
                                $learned_count += mysqli_fetch_assoc($result_a)['cnt'];
                            }
                            if ($full_count > 0) {
                                $progress = round($learned_count * 100 / $full_count, 2);
                                echo "Temos progresas: $progress%";
                            } else {
                                echo "Klaida: nėra įrašų";
                            }
                        }
                        ?>
                    </td>
                </form>
            </tr>
        </table>

        <form method="post">
            <input type="submit" name="continue" id="continue" value="Mokytis" <?php if (
                                                                                    !isset($_SESSION['lang']) || !isset($_SESSION['level'])
                                                                                    || !isset($_SESSION['theme'])
                                                                                ) {
                                                                                    echo "disabled";
                                                                                } ?> />
            <input type="submit" name="view" id="view" value="Peržiūrėti žodyną" <?php if (
                                                                                        !isset($_SESSION['lang']) || !isset($_SESSION['level'])
                                                                                        || !isset($_SESSION['theme'])
                                                                                    ) {
                                                                                        echo "disabled";
                                                                                    } ?> />
            <input type="submit" name="send" id="send" value="Siųsti išmokimo lygio raportą" <?php if (
                                                                                                    !isset($_SESSION['lang']) && !isset($_SESSION['level'])
                                                                                                    && !isset($_SESSION['theme'])
                                                                                                ) {
                                                                                                    echo "disabled";
                                                                                                } ?> />
            <input type="submit" name="reset" id="reset" value="Atstatyti žodyno parametrus">
        </form>
        <?php

        function load_words()
        {
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $lang = $_SESSION['lang'];
            $level = $_SESSION['level'];
            $theme = $_SESSION['theme'];
            $user = $_SESSION['userid'];
            $sql = "SELECT Word.word, Word.translation, Word.id FROM Word"
                . " LEFT JOIN DictionaryWord ON DictionaryWord.fk_Wordid=Word.id"
                . " LEFT JOIN Dictionary ON DictionaryWord.fk_Dictionaryid=Dictionary.id"
                . " WHERE Dictionary.language=\"$lang\" AND Dictionary.level=\"$level\" AND Dictionary.theme=\"$theme\"";
            $result = mysqli_query($db, $sql);
            if (!$result) {
                echo "Žodynas neturi žodžių";
            } else {
                $_SESSION['words'] = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $wordid = $row['id'];
                    $sql_wordstats = "SELECT learned, timesWrong FROM WordStats"
                        . " WHERE WordStats.fk_Useruserid=\"$user\" AND WordStats.fk_Wordid=$wordid";
                    $result_wordstats = mysqli_query($db, $sql_wordstats);
                    if (!$result_wordstats || mysqli_num_rows($result_wordstats) == 0) {
                        $sql_insert = "INSERT INTO WordStats (timesWrong, fk_Wordid, fk_Useruserid, learned) VALUES (0, $wordid, \"$user\", 0)";
                        mysqli_query($db, $sql_insert);
                        array_push($_SESSION['words'], $row);
                    } else {
                        $row_wordstats = mysqli_fetch_assoc($result_wordstats);
                        if ($row_wordstats['learned'] == 0) {
                            array_push($_SESSION['words'], $row);
                        }
                    }
                    // TODO: reports based on learning percents
                }
            }
            shuffle($_SESSION['words']); //randomize word list
            $_SESSION['total_words'] = count($_SESSION['words']);
            $_SESSION['current_word'] = 0;
            $_SESSION['incorrect_words'] = array(); //incorrect from round 1
            $_SESSION['incorrect_words2'] = array(); //incorrect from round 2
        }
        function continue_fn()
        {
            load_words();
            if ($_SESSION['total_words'] > 0) {
                echo "<script>window.location.href='/app/learn.php'</script>";
                exit();
            } else {
                echo "Žodyne nėra neišmoktų žodžių.<br/>";
            }
        }

        function view_fn()
        {
            load_words();
            echo "<script>window.location.href='/app/view_dict.php'</script>";
            exit();
        }

        function send_report()
        {
            $mail = new PHPMailer(true);
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->CharSet = "utf-8"; // set charset to utf8
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
            $mail->Port = 587; // TCP port to connect to
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->isHTML(true); // Set email format to HTML

            $mail->Username = 'itprojetkas.kalbos@gmail.com'; // SMTP username
            $mail->Password = 'itprojektas'; // SMTP password

            $mail->setFrom('noreply@itprojektas.lt', 'noreply'); //Your application NAME and EMAIL

            $userid = $_SESSION['userid'];
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $sql = "SELECT email FROM User WHERE userid=\"$userid\"";
            $result = mysqli_query($db, $sql);
            $address = mysqli_fetch_assoc($result)['email'];
            $mail->addAddress($address); // Target email


            $message = "";
            if (isset($_SESSION['lang']) && !isset($_SESSION['level']) && !isset($_SESSION['theme'])) {
                $mail->Subject = $_SESSION['lang'] . " kalbos mokymosi raportas";
                $lang = $_SESSION['lang'];
                $sql = "SELECT id, level, theme FROM Dictionary WHERE language=\"$lang\" AND (public = 1 OR fk_Useruserid=\"$userid\")";
                $result = mysqli_query($db, $sql);
                $full_count = 0;
                $learned_count = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    $dictid = $row['id'];
                    $level = $row['level'];
                    $theme = $row['theme'];
                    $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
                    $result_a = mysqli_query($db, $sql);
                    $dict_count = mysqli_fetch_assoc($result_a)['cnt'];
                    $full_count += $dict_count;

                    $sql = "SELECT count(*) as cnt FROM DictionaryWord"
                        . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
                        . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
                        . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
                    $result_a = mysqli_query($db, $sql);
                    $dict_leared = mysqli_fetch_assoc($result_a)['cnt'];
                    $learned_count += $dict_leared;

                    if ($dict_count > 0) {
                        $message = $message . "Žodyno " . $level . " " . $theme . " mokėjimo lygis: " . round($dict_leared * 100 /  $dict_count, 2) . "% <br/>";
                    } else {
                        $message = $message . "Žodyne " . $level . " " . $theme . " nėra įrašų<br/>";
                    }
                }

                if ($full_count > 0) {
                    $progress = round($learned_count * 100 / $full_count, 2);
                    $message = $message . "Kalbos bendras progresas: $progress%";
                } else {
                    $message = $message . "Kalboje nepridėta jokių žodžių";
                }
            } else if (isset($_SESSION['level']) && !isset($_SESSION['theme'])) {
                $mail->Subject = $_SESSION['lang'] . 'k. ' . $_SESSION['level'] . ' lygio mokymosi raportas';
                $lang = $_SESSION['lang'];
                $level = $_SESSION['level'];
                $sql = "SELECT id, theme FROM Dictionary WHERE language=\"$lang\" AND level=\"$level\" AND (public = 1 OR fk_Useruserid=\"$userid\")";
                $result = mysqli_query($db, $sql);
                $full_count = 0;
                $learned_count = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    $dictid = $row['id'];
                    $theme = $row['theme'];
                    $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
                    $result_a = mysqli_query($db, $sql);
                    $dict_count = mysqli_fetch_assoc($result_a)['cnt'];
                    $full_count += $dict_count;

                    $sql = "SELECT count(*) as cnt FROM DictionaryWord"
                        . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
                        . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
                        . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
                    $result_a = mysqli_query($db, $sql);
                    $dict_leared = mysqli_fetch_assoc($result_a)['cnt'];
                    $learned_count += $dict_leared;

                    if ($dict_count > 0) {
                        $message = $message . "Žodyno " . $level . " " . $theme . " mokėjimo lygis: " . round($dict_leared * 100 /  $dict_count, 2) . "% <br>";
                    } else {
                        $message = $message . "Žodyne " . $level . " " . $theme . "Nėra įrašų<br/>";
                    }
                }
                if ($full_count > 0) {
                    $progress = round($learned_count * 100 / $full_count, 2);
                    $message = $message . "Kalbos bendras progresas: $progress%";
                } else {
                    $message = $message . "Kalboje nėra žodžių įrašų";
                }
            } else if (isset($_SESSION['theme'])) {
                $mail->Subject = $_SESSION['lang'] . ' k. ' . $_SESSION['level'] . ' lygio ' . $_SESSION['theme'] . ' temos žodyno mokymosi raportas';
                $lang = $_SESSION['lang'];
                $level = $_SESSION['level'];
                $theme = $_SESSION['theme'];
                $sql = "SELECT id FROM Dictionary WHERE language=\"$lang\" AND level=\"$level\" AND theme=\"$theme\" AND (public = 1 OR fk_Useruserid=\"$userid\")";
                $result = mysqli_query($db, $sql);
                while ($row = mysqli_fetch_assoc($result)) {
                    $dictid = $row['id'];
                    $sql = "SELECT count(*) as cnt FROM DictionaryWord WHERE fk_Dictionaryid=$dictid";
                    $result_a = mysqli_query($db, $sql);
                    $dict_count = mysqli_fetch_assoc($result_a)['cnt'];

                    $sql = "SELECT count(*) as cnt FROM DictionaryWord"
                        . " LEFT JOIN Word ON DictionaryWord.fk_Wordid = Word.id"
                        . " LEFT JOIN WordStats ON Word.id=WordStats.fk_Wordid"
                        . " WHERE WordStats.fk_Useruserid=\"$userid\" AND DictionaryWord.fk_Dictionaryid=$dictid AND WordStats.learned=1";
                    $result_a = mysqli_query($db, $sql);
                    $dict_leared = mysqli_fetch_assoc($result_a)['cnt'];
                    if ($dict_count > 0) {
                        $message = $message . "Žodyno " . $level . " " . $theme . " mokėjimo lygis: " . round($dict_leared * 100 /  $dict_count, 2) . "% <br />";
                    } else {
                        $message = $message . "Žodyne " . $level . " " . $theme . " nėra įrašų <br />";
                    }
                }
            }
            $mail->MsgHTML($message); // Message body
            $sent = $mail->send();
            if (!$sent) {
                echo "Klaida siunčiant laišką <br />";
            }
        }

        if (array_key_exists('continue', $_POST)) {
            continue_fn();
        }
        if (array_key_exists('view', $_POST)) {
            view_fn();
        }
        if (array_key_exists('send', $_POST)) {
            send_report();
        }
        if (array_key_exists('chooselang', $_POST)) {
            if ($_POST['lang'] != "default") {
                $_SESSION['lang'] = $_POST['lang'];
                $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                mysqli_set_charset($db, 'utf8');
                $lang = $_SESSION['lang'];
                $user = $_SESSION['userid'];
                $sql = "UPDATE User SET lastLanguage = \"$lang\" WHERE userid = \"$user\"";
                mysqli_query($db, $sql);
            }
            echo "<script>window.location.href='/index.php'</script>";
            exit();
        }
        if (array_key_exists('chooselevel', $_POST)) {
            if ($_POST['level'] != "default") {
                $_SESSION['level'] = $_POST['level'];
                $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                mysqli_set_charset($db, 'utf8');
                $level = $_SESSION['level'];
                $user = $_SESSION['userid'];
                $sql = "UPDATE User SET lastLevel = \"$level\" WHERE userid = \"$user\"";
                mysqli_query($db, $sql);
            }
            echo "<script>window.location.href='/index.php'</script>";
            exit();
        }
        if (array_key_exists('choosetheme', $_POST)) {
            if ($_POST['theme'] != "default") {
                $_SESSION['theme'] = $_POST['theme'];
                $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
                mysqli_set_charset($db, 'utf8');
                $theme = $_SESSION['theme'];
                $user = $_SESSION['userid'];
                $sql = "UPDATE User SET lastTheme = \"$theme\" WHERE userid = \"$user\"";
                mysqli_query($db, $sql);
            }
            echo "<script>window.location.href='/index.php'</script>";
            exit();
        }
        if (array_key_exists('reset', $_POST)) {
            unset($_SESSION['lang']);
            unset($_SESSION['level']);
            unset($_SESSION['theme']);
            $db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            mysqli_set_charset($db, 'utf8');
            $user = $_SESSION['userid'];
            $sql = "UPDATE User SET lastTheme = NULL, lastLanguage = NULL, lastLevel = NULL WHERE userid = \"$user\"";
            mysqli_query($db, $sql);
            echo "<script>window.location.href='/index.php'</script>";
            exit();
        }
        ?>
    </center>
</body>

</html>