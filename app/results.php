<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>IT Projektas</title>
    <meta name="description" content="Užsienio kalbų žodžių mokymosi aplinka" />
    <meta name="author" content="Kristupas Talačka IFF-8/2" />
</head>

<body>
    <center>
        <h1>Mokymosi rezultatai</h1>
        <p>Baigėte mokytis šį žodyną!</p>
        <form method="post">
            <input type="submit" name="home" value="Grįžti" />
            <input type="submit" name="generate" value="Generuoti raportą" />
        </form>
        <?php

        function go_home()
        {
            unset($_SESSION['lang']);
            unset($_SESSION['level']);
            unset($_SESSION['theme']);
            unset($_SESSION['words']);
            unset($_SESSION['total_words']);
            unset($_SESSION['current_word']);
            unset($_SESSION['incorrect_words']);
            unset($_SESSION['incorrect_words2']);
            header('Location: /index.php');
            exit();
        }
        function generate_report()
        {
            header('Location: report.php');
            exit();
        }

        if (array_key_exists('home', $_POST)) {
            go_home();
        }
        if (array_key_exists('generate', $_POST)) {
            generate_report();
        }
        ?>
    </center>
</body>

</html>